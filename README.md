## SNP2PPI

**_SNP2PPI_**  offers users the ability to **map** single nucleotides polymorphisms _(SNPs)_ onto protein structures. 

The text below explains the user interface.

### Landing Page

The [main page](http://snp2ppi.bii.a-star.edu.sg) presents the user with a search box. This search box can be used to query the server for:

1. PDB file using the four letter PDB identifier
2. PDB chain combination. Spaces are not allowed, instead the user is recommended to search the PDB chain combination using the following format, "1hv4_A", where 1hv4 is the PDB-ID and "A" is the chain name. 
3. Uniprot Identifier. 
4. Metadata is stored for all entries present in this resource. Keywords can be used to search through the metadata. 

Note that only exact matches are supported for now. Also note that all searches are case sensitive. So if your search does not return a seaarch result, try again using a different case. 

##### Recommeded Usage

Although the data can be searched using different parameters (see above), the most optimal way of searching SNP2PPI is through the use of Uniprot identifier. 
Knowing before hand the Uniprot identifier to which your SNPs are quantified against will ensure that SNPs can be mapped to a structure (where available). 

#### Search Results

A successful search will populate and return a six column table, where the columns will list:

1. Uniprot Identifier
2. Gene Name
3. Gene Entry
4. PDB
5. Chain
6. Protein chain description

The search quary will match one of the six column entries. An unsuccessful search will return an empty table.

Underneath the table, the user is presented with three text input areas, requiring a Uniprot identifier, a PDB identifier and a chain. The entries in these text boxes are limited to 20 characters (Uniprot), four characters (PDB) and two characters (Chain). 

The text boxes can be filled manually or by clicking a row from the table. Clicking an entry from the row is preferred as it ensures that data for that entry is present in SNP2PPI. 

Additionally, a file containing SNP (missense) information exported from gnomAD or CHORUS is needed. This entry assumes files have been directly exported from gnomAD/CHORUS. Custom files can also be provided. If following the custom format, please follow the format below. 

###### **CUSTOM FILE FORMAT**
Two kinds of formats are acceptable. 

1. **Format - 1**:
    This format comes directly out of gnomAD and CHORUS. SNP data files exported directly [as of 25th June, 2020] in csv format can be provided to SNP2PPI. These files contain several columns, which contain the SNP, combined population frequencies and other cohort specific statistics. SNP2PPI has internal parsers which can convert this output into data usable with SNP2PPI. To ensure that the file is detected as an export from gnomAD and CHORUS, file names of the exported CSVs should not be changed, otherwise they will not be detected under format - 1. To enable user to provide data other than that sourced from gnomAD and CHORUS, it can be uploaded using Format -2, explained below. 

2. **Format - 2**:
    The user can also upload the data in a simple format other than that of gnomAD and CHORUS. For this, a minimum of two tab-delimited columns are required. With the first column listing SNP and the second column and onwards listing observation frequencies. Each column must have a header, with the first column having the header SNP and the following columns having informative labels, as these would be used in the feature viewer. Spaces in the column labels are not supported. The first line of the file must have a predefined keyword "#Custom", to tell the program the file has a custom format. An example preview is listed below.

    **Example Custom SNP file**  
    _#Custom_  
    _SNP<TAB>CohortLabel1<TAB>CohortLabel2<TAB>CohortLabel3_  
    _p.ALA2GLY<TAB>0.001<TAB>0.001<TAB>0.001_   
    
    The SNP column must list the amino acids using three-letter notation and using upper case. The SNP must also be preceeded by **p.**. While the exact numeric value is not important for visualizing the SNP in the structure, it is used for annotation in the protein feature viewer. If frequencies are not available, at least one column must be provided, even if the values are all "0.0". 
    
#### Visualization

Once the search terms and the SNP file have been submitted and successfully processed, a data visualization page is loaded. In this page two visualization states are loaded, one for protein structure data [NGL Graphics window on the left] and the other for protein sequence data [protein feature viewer on the right]. 

In case of a multimeric structure, the interface residues are represented in surface representation. Above the visualization window is a cohort selection dropdown, which will get populated with the header porvided for population frequencies in the SNP data. Selection from within this dropdown will show the SNPs on the structure of choice [The chain color of which will be blue]. 

The feature viewer can be expanded to list, SNPs per cohort and the region in the chosen structure that aligns with the canonical uniprot sequence. The SNPs by cohort are mapped to their respective positions in the sequence extracted from the structure. A single click on the mapped SNPs will reveal the change and its observed frequency as noted in the SNP file. 

#### Data available for download

Once the visualization page has been loaded, a link toward centre-top of the page will allow the user to download the data calculated for generating the visualization. Data is wrapped in a single Zip file which contains the following files:

1. The PDB structure chosen by the user from the table entries. **[file-name.pdb]**
2. The sequence of the uniprot identifier chosen along with the PDB structure from the table entries. **[uniprot-id.sq]**
3. Simplified representation of the SNP data if extracted from gnomAD/CHORUS or the same data if a custom SNP format was provided. **[SNP.DATA]**
4. In case of multimeric assemblies the interface residues shown in surface representation are provided. If the structure has four chains, A, B, C, D, and the user chooses chain A from the table entries, the file will contain the interface residues of chain A-B, A-B, A-C, A-D only and not all interfaces as the SNP data is only to be mapped on chain A. **[interface.residues]**
5. The mapping from SNP.DATA onto the structure (using the sequence as a reference), as shown in the feature viewer, and populated in the select-cohort drop-down is provided. The mapping will list the SNP data, the position of the SNP as calculated in the PDB file, along with the user provided population frequencies listed in the feature viewer. **[For.STR]** 
6. An output table **[Output.Table]** resulting from the mapping is provided. This table has five fixed columns which are:
a.  Position of each amino acid in the uniprot sequence 
b. The amino acid at the position (a.)
c. All SNPs provided by the user include numeric positions. The SNPs in one letter amino acid donation are listed in column 3.
d. The same entry in column c, is listed in three letter notation in column 4. This is essentially the same data provided by the user. 
e. Result of the match between a reference amino acid in the SNP, provided by the user, and the reference amino acid at the uniprot sequence match. "0" if there is no match and "1" in case there is a match. The mismatch would result if the SNPs were computed using an isoform other than the canonical isoform selected by uniprot. In case of a match, "1", the following columns will hold the population frequencies for all cohorts listed in the user input. The number of columns after the five fixed columns will depend on the number of cohorts listed in the SNP file. 
7. A simple text file is also produced listing the summary of the mapping in multiple sections **[Summary.Table]**. With the first section summarising the number of SNPs read, mapped and unmapped. The following sections list which of the SNPs were mapped and which were not. At the end of the file, a warning is dispalyed if during the sequence alignment process gaps were introduced. This is caused by the structure having missing residues [This error might also result if the structure is complete, and a peptide-ligand has the same chain label as selected by the user and a gap in residue numbers exists in the PDB, between protein chain residue numbers and the peptide-ligand. The method used here makes selections within multimeric assemblies using chain identifiers and therefore this gap between the primary protein chain and the peptide-ligand will be interpreted as "missing residues" and in this case - this warning can be ignored]. In this case manual inspection might be needed. The results that are to be inspected are listed in a file explained in the next bullet. Additionally at the end of this file, the total number of residues finally mapped to the structure (content of For.STR) are listed.
8. The results to be inspected are listed in "**Inspect.log**". The file has six columns, listing:
a. The amino acid number in the uniprot sequence
b. The amino acid at the position (a.)
c. The amino acid number from the sequence extracted from the structure
d. The amino acid at the position (c.)
e. The amino acid number extracted from the protein structure residue numbering
f. The amino acid at the position (e.)
Residues in (d.) and (f.) will be identical but the numbering in (c.) and (e.) might be different. The sequence in (d.) is aligned with the uniprot sequence (b.). Any gaps that are introduced wil be listed as "-" and may disrupt numbering in ways that are unexpected. While care has been taken, the user is requested to inspect this before trusting the results in the visualization. 

#### Possible sources of errors

In the current scheme there is no checking mechanism in place if the user tries to map the SNP data to the wrong reference protein or instead of selecting the entry from the table, manually enters an incorrect uniprot identifier and PDB chain combination. Some signs that may appear in case of a mismatch between the uniprot identifier and the PDB is that the PDB region aligned in the  feature viewer might not get populated, or in the earlier cases very few SNPs get mapped to the structure [Even if the SNPs are mapped to the wrong reference uniprot protein, there is still a chance that some residues might randomly match]. 

Other errors are captured and display potential causes leading to those errors.

### Found a bug: 
If something is broken or breaks during your interaction, please contact: [Ashar Malik](mailto:asharjm@bii.a-star.edu.sg)











